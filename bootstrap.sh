#!/usr/bin/env bash

set -e

cd "$(dirname "$0")";

BACKUP_DIR=~/opt/dotfiles.bak."$(date +%s)"

echo "Creating backup $BACKUP_DIR"
mkdir -p "$BACKUP_DIR"
for f in .[^.]*; do
    if [ "$f" != ".git" ] && [ -f ~/"$f" ]; then
        cp -r ~/"$f" "$BACKUP_DIR"
    fi
done

mkdir -p ~/.vim_swap

if [ ! -d ~/.fzf ]; then
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install --no-bash
fi

rsync --exclude '.git/' \
    --exclude 'bootstrap.sh' \
    -avh --no-perms . ~

source ~/.bashrc
