#!/bin/bash

cd ~/.vim/bundle

for i in `ls`; do
  cd "$i"
  echo "Updating $i..."
  git checkout .
  git pull
  cd ..
done
