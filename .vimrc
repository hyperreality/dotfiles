" Plugin Config {{{
execute pathogen#infect()

let g:syntastic_always_populate_loc_list = 1 " can immediately use :lnext / :lprev
let g:syntastic_python_checkers = ['python']
let g:syntastic_python_python_exec = 'python3'

set tags=./tags;/ " start in the current directory for "tags", and work up the tree towards root until one is found

let g:rg_command = '
  \ rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --color "always"
  \ -g "*.{js,json,php,md,styl,jade,html,config,py,cpp,c,go,hs,rb,conf}"
  \ -g "!{.git,node_modules,vendor}/*" '

command! -bang -nargs=* F call fzf#vim#grep(g:rg_command .shellescape(<q-args>), 1, <bang>0)
" }}}
" General {{{
set nocompatible 	" disable backwards compatibility mode
set hidden 		" allows switching buffers without saving every time
set shortmess+=I " remove splash

set directory=~/.vim_swap// " swapfile dir

" Theme
set t_Co=256
syntax on
set background=dark
colorscheme desert

" Transparent background in GVim?
hi Normal ctermbg=none
highlight NonText ctermbg=none

" Show long lines
:set display+=lastline 

" look for header files in the following directories when using gf
let &path.="src/include,/usr/include/AL,"

" }}}
" Folding {{{
set foldmethod=marker
set foldnestmax=10
set foldlevel=0

setlocal foldmethod=marker
" }}}
" Filetypes {{{

if has("autocmd")
    filetype plugin indent on

    map <F9> :call CompileRunGcc()<CR>
    func! CompileRunGcc()
    exec "w"
    if &filetype == 'c'
    exec "!gcc % -lcrypto -o %< && ./%<"
    elseif &filetype == 'cpp'
    exec "!g++ % -o %< && ./%<"
    elseif &filetype == 'java'
    exec "!javac % && java -cp %:p:h %:t:r"
    elseif &filetype == 'javascript'
    exec "!nodejs %"
    elseif &filetype == 'sh'
    exec "!bash %"
    elseif &filetype == 'python'
    exec "!python3 %"
    elseif &filetype == 'html'
    exec "!firefox % &"
    elseif &filetype == 'go'
    exec "!go build %< && go run %"
    elseif &filetype == 'mkd'
    exec "!~/.vim/scripts/Markdown.pl % > %.html &"
    exec "!firefox %.html &"
    elseif &filetype == 'tex'
    exec "!make && make clean"
    endif
    endfunc

    " Correct spacing and tabbing
    " Syntax of these languages is fussy over tabs Vs spaces
    autocmd FileType make setlocal ts=8 sts=8 sw=8 noexpandtab
    autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

    " Customisations based on house-style (arbitrary)
    autocmd FileType html setlocal ts=2 sts=2 sw=2 expandtab
    autocmd FileType css setlocal ts=2 sts=2 sw=2 expandtab
    autocmd FileType javascript setlocal ts=2 sts=2 sw=2 expandtab

    " Mutt
    autocmd BufNewFile,BufRead /tmp/mutt* set noautoindent filetype=mail wm=0 tw=78 nonumber digraph nolist
    autocmd BufNewFile,BufRead ~/tmp/mutt* set noautoindent filetype=mail wm=0 tw=78 nonumber digraph nolist

    " Treat .rss files as XML
    autocmd BufNewFile,BufRead *.rss setfiletype xml    
    autocmd BufNewFile,BufRead *.ejs setfiletype=html

endif
" }}}
" Leader {{{
let mapleader = "\<Space>"
nnoremap <Leader>w :w<CR>

" Paste shortcuts
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P

" enter visual line mode with space space
nmap <Leader><Leader> V
" }}}
" Layout and Movement {{{
set cursorline          " highlight current line
set lazyredraw          " redraw only when we need to.

set tabstop=4 " show existing tab with 4 spaces width
set shiftwidth=4 " when indenting with '>', use 4 spaces width
set expandtab " On pressing tab, insert 4 spaces
set softtabstop=4   " number of spaces in tab when editing

" jj is escape
inoremap jj <esc>

" map semicolon to colon
" map ; :
" noremap ;; ;

" move vertically by visual line
nnoremap j gj
nnoremap k gk

" highlight last inserted text
nnoremap gV `[v`]
" }}}
" Searching {{{
set showmatch           " highlight matching [{()}]
set incsearch           " search as characters are entered
set hlsearch            " highlight matches

" Searches will be case insentitive if the search is all lowercase characters and case sensitive otherwise.
set ignorecase
set smartcase

" }}}
" Writing {{{
" Pressing ,sc will toggle and untoggle spell checking
nnoremap <leader>sc :setlocal spell!<cr>
" Shortcuts using <leader>
nmap <leader>sn ]s
nmap <leader>sp [s
nmap <leader>sa zg
nmap <leader>s? z=

set langmenu=en_US

" Insert datestamp with F5
:nnoremap <F5> "=strftime("%c")<CR>P
:inoremap <F5> <C-R>=strftime("%c")<CR>
" }}}

