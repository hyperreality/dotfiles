#!/bin/bash

set -e

cd ~/.vim/bundle

git clone --depth=1 https://github.com/vim-syntastic/syntastic.git
git clone https://tpope.io/vim/commentary.git
git clone https://tpope.io/vim/surround.git
git clone https://github.com/sheerun/vim-polyglot
git clone https://github.com/tpope/vim-fugitive
